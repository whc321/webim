package com.changkang.webim.common;

import com.jfinal.render.JsonRender;
import com.jfinal.render.Render;
import com.jfinal.render.RenderFactory;

/**
 * 重写render方法,全局异常捕捉
 * @author 常康
 *
 */
public class HelloRenderFactory extends RenderFactory {

	@Override
	public Render getErrorRender(int errorCode) {
		if(errorCode==ResutlCode.NOTFOUND.code()) {
			return new JsonRender(Result.ret(errorCode, ResutlCode.NOTFOUND.description()));
		}else if(errorCode==ResutlCode.SERVERERROR.code()){
			return new JsonRender(Result.ret(errorCode, ResutlCode.SERVERERROR.description()));
		}else {
			return super.getErrorRender(errorCode);
		}
		
	}
}
