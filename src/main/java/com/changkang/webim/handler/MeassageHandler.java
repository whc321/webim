package com.changkang.webim.handler;

import com.alibaba.fastjson.JSONObject;
import com.changkang.webim.common.MessageType;
import com.changkang.webim.common.Result;
import com.changkang.webim.model.User;
import com.changkang.webim.mqtt.MqttKit;
import com.jfinal.kit.JsonKit;

/**
 * 所有消息的处理器
 * @author 常康
 *
 */
public class MeassageHandler {
 public final static MeassageHandler message=new MeassageHandler();
 
/**
 * 收到下线通知
 * @param message
 */
 public void changeStatus(JSONObject message) {
	 String userid=message.getString("userid");
	 //修改数据库状态
	 User.dao.changeStatus(userid, "hide");
	 //通知所有在线好友
	 for (User user : User.dao.onlineFriend(userid)) {
		 MqttKit.sendMessage(user.getId(), JsonKit.toJson(Result.success().add("type", MessageType.hide.type()).add("user", User.dao.findById(userid))));
	}
 }
}
