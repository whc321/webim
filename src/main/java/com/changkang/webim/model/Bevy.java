package com.changkang.webim.model;

import java.util.List;

import com.changkang.webim.model.base.BaseBevy;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;


@SuppressWarnings("serial")
public class Bevy extends BaseBevy<Bevy> {
	public static final Bevy dao = new Bevy().dao();
	
	public List<Bevy> bevys(String userid){
		return dao.find(Db.getSql("user.bevy"),userid);
	}
	
	public Page<Bevy> lookup(String content,int pageNo,String userid){
		SqlPara  sqlPara = Db.getSqlPara("user.lookupGroup",content,userid);
		return dao.paginate(pageNo, 8, sqlPara);
	}
}
