package com.changkang.webim.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseNotify<M extends BaseNotify<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setType(java.lang.Integer type) {
		set("type", type);
		return (M)this;
	}
	

	public java.lang.Integer getType() {
		return getInt("type");
	}

	public M setFrom(java.lang.String from) {
		set("from", from);
		return (M)this;
	}
	
	public java.lang.String getFrom() {
		return getStr("from");
	}

	public M setTo(java.lang.String to) {
		set("to", to);
		return (M)this;
	}
	
	public java.lang.String getTo() {
		return getStr("to");
	}

	public M setStatus(java.lang.Integer status) {
		set("status", status);
		return (M)this;
	}
	
	public java.lang.Integer getStatus() {
		return getInt("status");
	}

	public M setRemark(java.lang.String remark) {
		set("remark", remark);
		return (M)this;
	}
	
	public java.lang.String getRemark() {
		return getStr("remark");
	}

	public M setTimestamp(java.lang.String timestamp) {
		set("timestamp", timestamp);
		return (M)this;
	}
	
	public java.lang.String getTimestamp() {
		return getStr("timestamp");
	}

	public M setHandlerid(java.lang.String handlerid) {
		set("handlerid", handlerid);
		return (M)this;
	}
	
	public java.lang.String getHandlerid() {
		return getStr("handlerid");
	}

	public M setGroupid(java.lang.String groupid) {
		set("groupid", groupid);
		return (M)this;
	}
	
	public java.lang.String getGroupid() {
		return getStr("groupid");
	}

}
