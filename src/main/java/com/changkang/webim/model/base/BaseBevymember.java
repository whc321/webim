package com.changkang.webim.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseBevymember<M extends BaseBevymember<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setBevyid(java.lang.String bevyid) {
		set("bevyid", bevyid);
		return (M)this;
	}
	
	public java.lang.String getBevyid() {
		return getStr("bevyid");
	}

	public M setUseid(java.lang.String useid) {
		set("useid", useid);
		return (M)this;
	}
	
	public java.lang.String getUseid() {
		return getStr("useid");
	}

	public M setRemark(java.lang.String remark) {
		set("remark", remark);
		return (M)this;
	}
	
	public java.lang.String getRemark() {
		return getStr("remark");
	}

	public M setTimestamp(java.lang.Long timestamp) {
		set("timestamp", timestamp);
		return (M)this;
	}
	
	public java.lang.Long getTimestamp() {
		return getLong("timestamp");
	}

	public M setStatus(java.lang.Integer status) {
		set("status", status);
		return (M)this;
	}
	
	public java.lang.Integer getStatus() {
		return getInt("status");
	}

	public M setType(java.lang.Integer type) {
		set("type", type);
		return (M)this;
	}
	
	public java.lang.Integer getType() {
		return getInt("type");
	}

}
