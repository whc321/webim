package com.changkang.webim.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseFriend<M extends BaseFriend<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setGroupid(java.lang.String groupid) {
		set("groupid", groupid);
		return (M)this;
	}
	
	public java.lang.String getGroupid() {
		return getStr("groupid");
	}
	public M setBelong(java.lang.String belong) {
		set("belong", belong);
		return (M)this;
	}
	
	public java.lang.String getBelong() {
		return getStr("belong");
	}

	public M setUserid(java.lang.String userid) {
		set("userid", userid);
		return (M)this;
	}
	
	public java.lang.String getUserid() {
		return getStr("userid");
	}

	public M setRemark(java.lang.String remark) {
		set("remark", remark);
		return (M)this;
	}
	
	public java.lang.String getRemark() {
		return getStr("remark");
	}

	public M setTimestamp(java.lang.Long timestamp) {
		set("timestamp", timestamp);
		return (M)this;
	}
	
	public java.lang.Long getTimestamp() {
		return getLong("timestamp");
	}

}
