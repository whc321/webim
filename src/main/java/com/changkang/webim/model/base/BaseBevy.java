package com.changkang.webim.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseBevy<M extends BaseBevy<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setGroupname(java.lang.String groupname) {
		set("groupname", groupname);
		return (M)this;
	}
	
	public java.lang.String getGroupname() {
		return getStr("groupname");
	}

	public M setBelong(java.lang.String belong) {
		set("belong", belong);
		return (M)this;
	}
	
	public java.lang.String getBelong() {
		return getStr("belong");
	}

	public M setAvatar(java.lang.String avatar) {
		set("avatar", avatar);
		return (M)this;
	}
	
	public java.lang.String getAvatar() {
		return getStr("avatar");
	}

	public M setDes(java.lang.String des) {
		set("des", des);
		return (M)this;
	}
	
	public java.lang.String getDes() {
		return getStr("des");
	}

	public M setStatus(java.lang.String status) {
		set("status", status);
		return (M)this;
	}
	
	public java.lang.String getStatus() {
		return getStr("status");
	}

	public M setNumber(java.lang.Integer number) {
		set("number", number);
		return (M)this;
	}
	
	public java.lang.Integer getNumber() {
		return getInt("number");
	}

	public M setTimestamp(java.lang.Long timestamp) {
		set("timestamp", timestamp);
		return (M)this;
	}
	
	public java.lang.Long getTimestamp() {
		return getLong("timestamp");
	}

}
