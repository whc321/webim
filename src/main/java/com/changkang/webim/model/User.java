package com.changkang.webim.model;


import java.util.List;

import com.changkang.webim.model.base.BaseUser;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;


@SuppressWarnings("serial")
public class User extends BaseUser<User> {
	public static final User dao = new User().dao();
	
	public User login(String phone,String password) {
		return dao.findFirst(Db.getSql("user.login"),phone,password);
	}
	
	public User phone(String phone) {
		return dao.findFirst(Db.getSql("user.phone"),phone);
	}
	
	public boolean changeStatus(String id,String status) {
		return dao.findById(id).set("status", status).update();
	}
	
	
	
	public List<User> onlineFriend(String id){
		return dao.find(Db.getSql("user.online"),id);
	}
	
	public List<User> friends(String groupid){
		return dao.find(Db.getSql("user.friends"),groupid);
	}
	
	public Page<User> lookup(String content,int pageNo,String userid){
		SqlPara  sqlPara = Db.getSqlPara("user.lookupFriend",content,userid);
		return dao.paginate(pageNo, 8, sqlPara);
	}
	
}
