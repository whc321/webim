package com.changkang.webim.model;

import java.util.List;

import com.changkang.webim.model.base.BaseNotify;
import com.jfinal.plugin.activerecord.Db;

/**
 * 申请消息通知
 * @author ck
 *
 */
@SuppressWarnings("serial")
public class Notify extends BaseNotify<Notify> {
	public static final Notify dao = new Notify().dao();
	
	public List<Notify> checkNotify(String from,String to,int type){
		return dao.find(Db.getSql("user.checkNotify"),from,to,type);
	}
	
	public List<Notify> getNotify(String handlerid){
		return dao.find(Db.getSql("user.getNotify"),handlerid);
	}
	
	
}
