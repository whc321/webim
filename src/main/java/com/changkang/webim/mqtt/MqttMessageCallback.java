package com.changkang.webim.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import com.alibaba.fastjson.JSONObject;
import com.changkang.webim.handler.MeassageHandler;
import com.jfinal.kit.LogKit;
/**
 * 自定义mqtt消息回调处理类
 * 有两种方案可以实现聊天系统
 * 1.利用这个回调类：mqtt接收，server解析，mqtt转发
 * 2.利用http接口：http接收，server解析，mqtt转发
 * @author 常康
 *
 */
public class MqttMessageCallback  implements MqttCallback{
    /**
     * 连接中断，意外掉线回调(服务器挂掉，断电，断网，等等)
     */
	@Override
	public void connectionLost(Throwable cause) {
		LogKit.error("server端连接mqtt服务器失败",cause);
	}
    
	/**
	 * 收到消息回调
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		JSONObject messageJson=JSONObject.parseObject(new String(message.getPayload()));
		switch (messageJson.getInteger("type")) {
		
		case 6: //在线/不在线类型消息
			MeassageHandler.message.changeStatus(messageJson);
				break;
		case 1: //聊天类型消息
				//MeassageHandler.handler.chatMessage(object);
				break;
		case 2: //修改个性签名类型消息
				//MeassageHandler.handler.modifySign(object);
				break;
		case 500://服务器遗愿消息消息    
				System.out.println("服务器收到自己的掉线信息，目前不做处理");
				break;
		default:
				break;
		}
	}
    
	/**
	 * 消息发送成功回调
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		if(!token.isComplete()) {
			LogKit.error("消息发送到mqtt服务器失败");
		}
		
	}

}
