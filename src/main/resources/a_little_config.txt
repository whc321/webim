#数据库连接地址
jdbc.url = 数据库连接地址

#数据库连接账号
jdbc.user = 数据库账号

#数据库连接密码
jdbc.password =数据库密码

#是否打开开发者模式
jfinal.devMode = true

#MQTT Broker连接地址 
mqtt.brokerURL=MQTT服务器地址

#MQTT ClientId  id设置为空，可以防止同时登录冲突，导致挤掉线
mqtt.clientId="" 

#MQTT Client连接MQTT Broker时使用的用户名
mqtt.userName=MQTT服务器连接账号

#MQTT Client连接MQTT Broker时使用的密码
mqtt.password=MQTT服务器连接密码

#默认人topic
mqtt.serverTopic=自定义服务器主题

#是否自动重连 默认false
mqtt.automaticReconnection=false

#是否清理session，false时可接收离线消息 true忽略离线消息 默认true
mqtt.cleanSession=true

#连接MQTT Broker超时时间 默认:30s
mqtt.connectionTimeout=30

#MQTT 心跳时间间隔 默认60s
mqtt.keepAliveInterval=60

#遗嘱消息主题
mqtt.willTopic=a

#遗嘱消息内容
mqtt.willPayload=a

#MQTT 消息保存方式 如果不设置，默认保存在内存中，设置了则保存着指定的目录下
mqtt.stroageDir=/root

#是否手动发送消息应答 该设置只在Qos=1有效 默认false
mqtt.manualAcks=false

#遗嘱消息是否保留
mqtt.willretained=false

#新注册用户默认头像
user.headimg=upload/defalut.jpg

#新注册用户默认签名
user.sign=该用户很懒，还没有签名！