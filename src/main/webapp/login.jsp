<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
 <link rel="icon" href="static/images/favicon.ico" type="image/x-icon" />
<title>WebIM</title>
<link rel="stylesheet" href="static/css/layui.css">
<style type="text/css">
		.login-page {
		  width: 360px;
		  padding: 8% 0 0;
		  margin: auto;
		}
		.form {
		  position: relative;
		  z-index: 1;
		  background: #FFFFFF;
		  max-width: 360px;
		  margin: 0 auto 100px;
		  padding: 45px;
		  text-align: center;
		  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
		}
		.form input {
		  font-family: "Roboto", sans-serif;
		  outline: 0;
		  background: #f2f2f2;
		  width: 100%;
		  border: 0;
		  margin: 0 0 15px;
		  padding: 15px;
		  box-sizing: border-box;
		  font-size: 14px;
		}
		.form button {
		  font-family: "Microsoft YaHei","Roboto", sans-serif;
		  text-transform: uppercase;
		  outline: 0;
		  background: #4CAF50;
		  width: 100%;
		  border: 0;
		  padding: 15px;
		  color: #FFFFFF;
		  font-size: 14px;
		  -webkit-transition: all 0.3 ease;
		  transition: all 0.3 ease;
		  cursor: pointer;
		}
		.form button:hover,.form button:active,.form button:focus {
		  background: #43A047;
		}
		.form .message {
		  margin: 15px 0 0;
		  color: #b3b3b3;
		  font-size: 12px;
		}
		.form .message a {
		  color: #4CAF50;
		  text-decoration: none;
		}
		.form .register-form {
		  display: none;
		}
		.container {
		  position: relative;
		  z-index: 1;
		  max-width: 300px;
		  margin: 0 auto;
		}
		.container:before, .container:after {
		  content: "";
		  display: block;
		  clear: both;
		}
		.container .info {
		  margin: 50px auto;
		  text-align: center;
		}
		.container .info h1 {
		  margin: 0 0 15px;
		  padding: 0;
		  font-size: 36px;
		  font-weight: 300;
		  color: #1a1a1a;
		}
		.container .info span {
		  color: #4d4d4d;
		  font-size: 12px;
		}
		.container .info span a {
		  color: #000000;
		  text-decoration: none;
		}
		.container .info span .fa {
		  color: #EF3B3A;
		}
		body {
		  background: #76b852; /* fallback for old browsers */
		  background: -webkit-linear-gradient(right, #76b852, #8DC26F);
		  background: -moz-linear-gradient(right, #76b852, #8DC26F);
		  background: -o-linear-gradient(right, #76b852, #8DC26F);
		  background: linear-gradient(to left, #76b852, #8DC26F);
		  font-family: "Roboto", sans-serif;
		  -webkit-font-smoothing: antialiased;
		  -moz-osx-font-smoothing: grayscale;      
		}
		.shake_effect{
		 	-webkit-animation-name: shake;
  			animation-name: shake;
  			-webkit-animation-duration: 1s;
  			animation-duration: 1s;
		}
		@-webkit-keyframes shake {
		  from, to {
		    -webkit-transform: translate3d(0, 0, 0);
		    transform: translate3d(0, 0, 0);
		  }

		  10%, 30%, 50%, 70%, 90% {
		    -webkit-transform: translate3d(-10px, 0, 0);
		    transform: translate3d(-10px, 0, 0);
		  }

		  20%, 40%, 60%, 80% {
		    -webkit-transform: translate3d(10px, 0, 0);
		    transform: translate3d(10px, 0, 0);
		  }
		}

		@keyframes shake {
		  from, to {
		    -webkit-transform: translate3d(0, 0, 0);
		    transform: translate3d(0, 0, 0);
		  }

		  10%, 30%, 50%, 70%, 90% {
		    -webkit-transform: translate3d(-10px, 0, 0);
		    transform: translate3d(-10px, 0, 0);
		  }

		  20%, 40%, 60%, 80% {
		    -webkit-transform: translate3d(10px, 0, 0);
		    transform: translate3d(10px, 0, 0);
		  }
		}
		p.center{
			color: #fff;font-family: "Microsoft YaHei";
		}
	</style>
</head>
<script src="static/layui.js"></script>
<script src="static/js/jquery-2.1.1.js"></script>
<script src="static/js/jquery.cookie.js"></script>
<body>
<%
if(session.getAttribute("user")!=null){
%>
 <script type="text/javascript"> 
 window.location.href = "chatView";
 </script>  
<%
}
%>
<div class="htmleaf-container">

		<div id="wrapper" class="login-page">
		 <div class="layui-container">
               <img src="static/images/logo.png"  height="73px"> 
          </div>
		  <div id="login_form" class="form">
		    <form class="register-form">
		    <input type="text" placeholder="昵称" id="r_user_username" maxlength="7"/>
		      <input type="number" placeholder="手机号" id="r_user_phone" maxlength="11"/>
		      <input type="password" placeholder="密码" id="r_password" maxlength="20"/>
		      <input type="password" placeholder="确认密码" id="r_check_password" maxlength="20"/>
		      <button id="create">创建账户</button>
		      <p class="message">已经有了一个账户? <a href="#">立刻登录</a></p>
		    </form>
		    <form class="login-form">
		      <input type="number" placeholder="手机号" id="user_name"  maxlength="11"/>
		      <input type="password" placeholder="密码" id="password"  maxlength="20">
		      <button id="login">登　录</button>
		      <p class="message">还没有账户? <a href="#">立刻创建</a></p>
		    </form>
		  </div>
		</div>
	</div>
	

	<script type="text/javascript">
	layui.use(['jquery','layer'], function(){
	var $=layui.jquery;
	var layer = layui.layer;
	if($.cookie('zhanghao')!=null){
		  $("#user_name").val($.cookie('zhanghao'));
	}	
	function check_login(){
	 var name=$("#user_name").val();
	 var pass=$("#password").val();
	 $.get("login",{"phone":name,"password":pass},function(res){
		 if(res.code==0){
			 $.cookie("zhanghao",name,{expires:364} );
			 window.location.href = "chatView";
		 }else{
			 Dialog(res.msg);
			 $("#login_form").removeClass('shake_effect');  
			  setTimeout(function(){
			   $("#login_form").addClass('shake_effect')
			  },1);  
		 }
	 });	
}
	function check_register(){
		var username = $("#r_user_username").val();
		var phone = $("#r_user_phone").val();
		var password = $("#r_password").val();
		var checkPassword = $("#r_check_password").val();
		if(password == checkPassword){
		   $.get("reg",{"user.username":username,"user.phone":phone,"user.password":password},function(res){
			   if(res.code==0){
				   layer.msg(res.msg, {icon: 6,time:1000,anim:6});
			   }else{
				      Dialog(res.msg);
					  $("#login_form").removeClass('shake_effect');  
					  setTimeout(function(){
					   $("#login_form").addClass('shake_effect')
					  },1);  
			   }
		   });
		 } else{
			 Dialog("两次密码不一样");
		  $("#login_form").removeClass('shake_effect');  
		  setTimeout(function(){
		   $("#login_form").addClass('shake_effect')
		  },1);  
		 }
	}
	$(function(){
		$("#create").click(function(){
			check_register();
			return false;
		})
		$("#login").click(function(){
			check_login();
			return false;
		})
		$('.message a').click(function () {
		    $('form').animate({
		        height: 'toggle',
		        opacity: 'toggle'
		    }, 'slow');
		});
	})
	
	function Dialog(content){
  		layer.msg(content, {icon: 5,time:1000,anim:6});
  	};
});
</script>
</body>
</html>