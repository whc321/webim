package com.changkang.webim.test;

import java.util.List;

import com.changkang.webim.common.MainConfig;
import com.changkang.webim.model.User;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.druid.DruidPlugin;
/**
 * 数据库查询测试类
 * @author 常康
 *
 */
public class ActiveRecordTest {
	
	    
	    public static void main(String[] args) {
	    	PropKit.use("a_little_config.txt");
	    	DruidPlugin dp = MainConfig.createDruidPlugin();
	    	ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
			arp.setBaseSqlTemplatePath(PathKit.getWebRootPath()+"/src/main/resources/sql");
			arp.addSqlTemplate("sql.txt");
	    	arp.addMapping("user", User.class);
	    	dp.start();
	    	arp.start();
	    	
	    	//根据对象查找
	    	List<User> users=User.dao.find(Db.getSql("user.find"));
	    	
	    	for (User user : users) {
				LogKit.info(user.toJson());
			}
	    	
	    	//根据id查找对象
	    	User user=User.dao.findById("1");
	    	LogKit.info(user.toJson());
	    	
	    	arp.stop();
	    	dp.stop();    	
		}
	    

	    
}
