package com.changkang.webim.test;

import com.changkang.webim.mqtt.MqttKit;
import com.changkang.webim.mqtt.MqttPlugin;
import com.jfinal.kit.PropKit;
/**
 * mqtt client测试类
 * @author 常康
 *
 */
public class MqttClientTest {
	
  public static void main(String[] args) {
	PropKit.use("a_little_config.txt");
	MqttPlugin mqttPlugin=new MqttPlugin();
	mqttPlugin.start();
	
	//测试mqtt client发送消息
	MqttKit.sendMessage("webim/875079028", "1234");
	
	mqttPlugin.stop();
}
}
